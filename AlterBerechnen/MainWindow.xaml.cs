﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace AlterBerechnen {
    /// 26.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        void doCalculations() {
            try {
                DateTime inputDate = new DateTime(Convert.ToInt32(Input_Date_TextBox.Text.Substring(6, 4)), Convert.ToInt32(Input_Date_TextBox.Text.Substring(3, 2)), Convert.ToInt32(Input_Date_TextBox.Text.Substring(0, 2)));
                TimeSpan differenceTime = DateTime.Now - inputDate;
                if (UnitSecond_Radio_Button.IsChecked == true) // second
                    Output_TextBox.Text = Convert.ToInt32(differenceTime.TotalSeconds).ToString();
                else if (UnitDay_Radio_Button.IsChecked == true) // day
                    Output_TextBox.Text = Convert.ToInt32(differenceTime.TotalDays).ToString();
                else if(UnitWeek_Radio_Button.IsChecked == true) // week
                    Output_TextBox.Text = Convert.ToInt32(differenceTime.TotalDays / 7).ToString();
                else if(UnitMonth_Radio_Button.IsChecked == true) // month
                    Output_TextBox.Text = Math.Round(Convert.ToDecimal(differenceTime.TotalDays / 30.44), 2).ToString();
                else if (UnitYear_Radio_Button.IsChecked == true) // year
                    Output_TextBox.Text = Math.Round(Convert.ToDecimal(differenceTime.TotalDays / 365.2425), 2).ToString();
            }
            catch {

            }
        }

        private void Input_Date_TextBox_TextChanged(object sender, TextChangedEventArgs e) {
            doCalculations();
        }

        private void Input_Date_TextBox_TextChanged(object sender, RoutedEventArgs e) {
            doCalculations();
        }
    }
}
