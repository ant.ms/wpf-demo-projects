﻿// 29.10.2019, Yanik Ammann, Demo Project

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Auftrag_1 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
		}

		private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e) {
			if (TextBlock.Text == "Hello World") {
				TextBlock.Text = "Click me!";
			} else {
				TextBlock.Text = "Hello World";
			}
		}
	}
}
