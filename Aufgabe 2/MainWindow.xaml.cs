﻿// 29.10.2019, Yanik Ammann, input that adds to list

using System.Windows;

namespace Aufgabe_2_1 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

		int counter = 1;
		public MainWindow() {
			InitializeComponent();
		}

		private void add_button_Click(object sender, RoutedEventArgs e) {
			if (inputBox.Text != "") {
				ListViewBox.Items.Add(counter + ". " + inputBox.Text);
				inputBox.Text = "";
				counter++;
				add_button.IsEnabled = false;
			}
		}

		private void cancel_button_Click(object sender, RoutedEventArgs e) {
			this.Close();
		}

		private void inputBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) {
			add_button.IsEnabled = !string.IsNullOrWhiteSpace(inputBox.Text);
		}
	}
}
