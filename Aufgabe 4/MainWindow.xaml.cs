﻿// 30.10.2019, Yanik Ammann, adds and removed entries to a list


using System;
using System.Windows;

namespace Aufgabe_4 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		int itemsinList = 0;
		int itemsinSelection = 4;
		public MainWindow() {
			InitializeComponent();
		}

		private void add_button_Click(object sender, RoutedEventArgs e) {
			// checkes what is selected and adds it to the list
			switch (cb_Food.SelectedIndex) {
				case 0: // apple
					lsview_Food.Items.Add("Apple");
					AppleEntry.IsEnabled = false;
					break;
				case 1: // Egg
					lsview_Food.Items.Add("Egg");
					EggEntry.IsEnabled = false;
					break;
				case 2: // tea
					lsview_Food.Items.Add("Tea");
					TeaEntry.IsEnabled = false;
					break;
				case 3: // coffee
					lsview_Food.Items.Add("Coffee");
					CoffeeEntry.IsEnabled = false;
					break;
				case 4: // bread
					lsview_Food.Items.Add("Bread");
					BreadEntry.IsEnabled = false;
					break;
			}
			cb_Food.SelectedIndex = -1;
			// enables button if list empty
			if (itemsinList == 0) {
				remove_button.IsEnabled = true;
			}
			itemsinList++;
			itemsinSelection--;
			if (itemsinSelection == 0) {
				add_button.IsEnabled = false;
			}
		}

		private void remove_button_Click(object sender, RoutedEventArgs e) {
			// checks what is selected and enable the entry again
			try {
				switch (lsview_Food.SelectedValue) {
				case "Apple": // apple
					AppleEntry.IsEnabled = true;
					break;
				case "Egg": // Egg
					EggEntry.IsEnabled = true;
					break;
				case "Tea": // tea
					TeaEntry.IsEnabled = true;
					break;
				case "Coffee": // coffee
					CoffeeEntry.IsEnabled = true;
					break;
				case "Bread": // bread
					BreadEntry.IsEnabled = true;
					break;
				}
				// removes it from the list
				lsview_Food.Items.Remove(lsview_Food.SelectedItems[0]);
			}
			catch {
				Console.WriteLine("ERROR: non selected");
			}
			itemsinList--;
			if (itemsinList == 0) {
				remove_button.IsEnabled = false;
			}
			itemsinSelection++;
			if (itemsinSelection != 0) {
				add_button.IsEnabled = true;
			}
		}

		private void lsview_Food_GotFocus(object sender, RoutedEventArgs e) {
			remove_button.IsDefault = true;
			add_button.IsDefault = false;
		}

		private void cb_Food_GotFocus(object sender, RoutedEventArgs e) {
			add_button.IsDefault = true;
			remove_button.IsDefault = false;
		}
	}
}
