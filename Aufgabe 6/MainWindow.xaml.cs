﻿// 30.10.2019, Yanik Ammann, a simple timer

using System;
using System.Windows;
using System.Windows.Threading;

namespace Aufgabe_6 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		int timeCounted = 0;
		bool running = false;
		bool started = false;
		bool firstRun = true;
		int LengthOfTimer = 10;


		public MainWindow() {
			InitializeComponent();
			tbCountdown.Text = LengthOfTimer.ToString();
		}

		private void start_button_Click(object sender, RoutedEventArgs e) {
			if (firstRun == true) {
				// first run
				DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
				dispatcherTimer.Tick += timer_Tick;
				dispatcherTimer.Interval = TimeSpan.FromMilliseconds(40); //100ms?
				dispatcherTimer.Start();
				firstRun = false;
				running = true;
				started = true;
				start_button.Content = "Pause";
				tbCountdown.IsReadOnly = true;
				LengthOfTimer = Convert.ToInt32(tbCountdown.Text);
			} 
			else {
				if (started == false) {
					// timer hasn't been started yet (or run out) => start again
					running = true;
					started = true;
					timeCounted = 0;
					tbCountdown.IsReadOnly = true;
					LengthOfTimer = Convert.ToInt32(tbCountdown.Text);
					start_button.Content = "Pause";
				} else {
					// timer is started (hasn't run out)
					if (running == true) {
						// started running => pause
						start_button.Content = "Continue";
						running = false;
						//tbCountdown.IsReadOnly = false;
					} else {
						// started and not running => resume
						start_button.Content = "Pause";
						running = true;
						tbCountdown.IsReadOnly = true;
					}
				}
			}
		}
		private void timer_Tick(object sender, EventArgs e) {
			if (running == true) {
				timeCounted++;

				if (timeCounted == LengthOfTimer * 10) {
					// finished timer
					start_button.Content = "Reset";
					running = false;
					started = false;
					tbCountdown.IsReadOnly = false;
					tbCountdown.Text = LengthOfTimer.ToString();
				} else {
					tbCountdown.Text = Convert.ToString((LengthOfTimer * 10 - timeCounted) / 10);
				}
			}
		}
	}
}
