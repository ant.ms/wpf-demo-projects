﻿// 04.11.2019, Yanik Ammann, A simple text editor

using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace Aufgabe_8 {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

		string path = "";

		public MainWindow() {
			InitializeComponent();
		}


		private void Open_Menu_Item_Click(object sender, RoutedEventArgs e) {
			// opens a file
			OpenFileDialog openFileDialog = new OpenFileDialog();
			openFileDialog.Filter = "text-files (.txt)|*.txt|markdown-files (.md)|*.md|all|*.*";
			openFileDialog.ShowDialog();
			try {
				using (StreamReader sr = new StreamReader(openFileDialog.FileName)) {
					Editor_Text_Box.Text = sr.ReadToEnd();
				}
			}
			catch {
				Console.WriteLine("ERROR: no file selected");
			}
		}

		private void Save_Menu_Item_Click(object sender, RoutedEventArgs e) {
			writeToFile(false);
		}

		public void writeToFile(bool saveAs) {
			// saves a file
			if (path == "" || saveAs == true) {
				SaveFileDialog saveFileDialog = new SaveFileDialog();
				saveFileDialog.Filter = "text-files (.txt)|*.txt|markdown-files (.md)|*.md|all|*.*";
				saveFileDialog.ShowDialog();
				path = saveFileDialog.FileName;
			}
			try {
				using (StreamWriter sw = new StreamWriter(path)) {
					sw.WriteLine(Editor_Text_Box.Text);
				}
			}
			catch {
				System.Windows.MessageBox.Show("Invalid File path");
			}
		}
		private void Save_As_Menu_Item_Click(object sender, RoutedEventArgs e) {
			writeToFile(true);
		}

		private void Close_Menu_Item_Click(object sender, RoutedEventArgs e) {
			this.Close();
		}
	}
}
