﻿///////////////////////////////////////////////////////////////////////////////////////////////////////
///																									///
///		Application Name:	Aufgabe Shoppinglist													///
///		Version:			v0.2																	///
///		Description:		Allows the user to generate a shopping list out of a list 				///
///							of available items and calculate the price of them						///
///		Author:				Yanik Ammann															///
///		DateLastChanged:	06.11.2019																///
///																									///
///////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace Aufgabe_Shoppinglist {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		int prize = 0;
		int prizeDifference = 0;
		// create Regex-entry to select every non-numerical character
		Regex digitsOnly = new Regex(@"[^\d]");

		public MainWindow() {
			InitializeComponent();
		}

		void activateListView(string company) {
			// change view by hinding and unhiding grids
			ShopSelectionGrid.Visibility = Visibility.Hidden;
			MainGrid.Visibility = Visibility.Visible;

			// add items to ComboBox
			cbItemSelect.Items.Add(company + " Bleistifte\t");
			cbItemSelect.Items.Add(company + " Block\t");
			cbItemSelect.Items.Add(company + " Pride-Flaggen");
			cbItemSelect.Items.Add(company + " Pizza\t");
			cbItemSelect.Items.Add(company + " Milch\t");
			cbItemSelect.Items.Add(company + " Spaghetti\t");
			cbItemSelect.Items.Add(company + " Armband\t");

			// add differenciation with price and products for different shops
			if (company == "Coop") {
				// Coop specifics
				prizeDifference = 2;
				cbItemSelect.Items.Add(company + " A4 Papier\t");
			} else if (company == "Migros") {
				// Migros specifics
				prizeDifference = 0;
				cbItemSelect.Items.Add(company + " Block\t");
			} else {
				// Denner specifics
				prizeDifference = 1;
				cbItemSelect.Items.Add(company + " Ice Tea\t");
			}
		}

		public void updateDisplay() {
			// displays price in TextBlock
			Total_Cost_Text_Block.Text = "CHF " + prize.ToString() + " ";
		}

		#region List View
		private void Add_button_Click(object sender, RoutedEventArgs e) {
			try {
				// adds item to listView if something is selected
				if (cbItemSelect.SelectedIndex != -1) {
					// calculate prices
					int tmp = cbItemSelect.SelectedIndex * 3 + 1 + prizeDifference;
					prize += tmp;
					// adds item to listView and spaces it correctly
					item_listView.Items.Add(cbItemSelect.SelectedValue + "\t\t\t\t\t\t\t\t\t\t\t\t\t" + tmp + ".-");
				}
			}
			catch {

			}
			updateDisplay();
		}

		private void Print_button_Click(object sender, RoutedEventArgs e) {
			// displays output in MessageBox, convert a lot of tabs to a few tabs
			string output = "";
			foreach (string element in item_listView.Items) {
				output += element.Replace("\t\t\t\t\t\t\t\t\t\t\t\t\t", "\t\t\t") + "\n";
			}
			// display total
			output += "\nTotal: \t\t\t\t\t " + prize + " .-";
			MessageBox.Show(output);
		}

		private void item_listView_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
			// REMOVE selected item from list if double clicked on the list entry
			
			// Removes the prize of the object in the list from total-prize, by taking the text of the selected list entry
			// and converting it into a string, deleting all non-numerical characters and then converting it to int 
			prize -= Convert.ToInt32(digitsOnly.Replace(item_listView.SelectedItems[0].ToString(), ""));
			item_listView.Items.Remove(item_listView.SelectedItems[0]);
			updateDisplay();
		}

		private void Close_button_Click(object sender, RoutedEventArgs e) {
			// close window
			this.Close();
		}

		private void Switch_Button_Click(object sender, RoutedEventArgs e) {
			// switches back to shop-select-view by hiding and unhiding grids, 
			// and clears entries in content-box (because different shops have different items)
			ShopSelectionGrid.Visibility = Visibility.Visible;
			MainGrid.Visibility = Visibility.Hidden;
			cbItemSelect.Items.Clear();
		}
		#endregion


		#region Shop Selection
		private void CoopIcon_MouseDown(object sender, MouseButtonEventArgs e) {
			// Coop was selected
			activateListView("Coop");
		}

		private void MigrosIcon_MouseDown(object sender, MouseButtonEventArgs e) {
			// Migros was selected
			activateListView("Migros");
		}

		private void DennerIcon_MouseDown(object sender, MouseButtonEventArgs e) {
			// Denner was selected
			activateListView("Denner");
		}
		#endregion
	}
}
