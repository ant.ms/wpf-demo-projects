﻿using System.Windows;
using System.Windows.Controls;

namespace CountVocals {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Input_Text_Box_TextChanged(object sender, TextChangedEventArgs e) {
            try {
                // count ammount of vocals (A,Ä,E,I,O,Ö,U,Ü,Y)
                int counter = 0;
                foreach (char item in Input_Text_Box.Text) {
                    string d = item.ToString().ToLower();
                    if (d == "a" || d == "ä" || d == "e" || d == "i" || d == "o" || d == "ö" || d == "u" || d == "ü" || d == "y") {
                        counter++;
                    }
                }
                Output_Text_Box.Text = counter.ToString();
            }
            catch {
                // do nothing (aka: relax)
            }
        }
    }
}
