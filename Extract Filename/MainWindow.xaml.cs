﻿using System.Windows;
using System.Windows.Controls;
using System.Text.RegularExpressions;

namespace Extract_Filename {
    /// 26.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Input_Text_Box_TextChanged(object sender, TextChangedEventArgs e) {
            try {
                Regex myRegex = new Regex(@"[A-Z|a-z]:\\.{0,255}\\");
                string input = myRegex.Replace(Input_Text_Box.Text, "");
                if (input.IndexOf(".") != -1) {
                    // take everything before first dot
                    input = input.Substring(0, input.IndexOf("."));
                }
                Output_Text_Box.Text = input;
            }
            catch {

            }
        }
    }
}
