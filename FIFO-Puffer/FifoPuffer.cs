﻿using System.Windows;
using System;

namespace FIFO_Puffer {

    // 03.12.2019, Yanik Ammann
    public partial class MainWindow {
        class FifoPuffer {
            int[] array;

            public FifoPuffer (int sizeOfPuffer) {
                array = new int[sizeOfPuffer];
                // set everything to -1
                for (int i = 0; i < array.Length; i++) {
                    array[i] = -1;
                }
            }

            public int Get() {
                int output = array[0];
                // schift array
                for (int i = 0; i < array.Length - 1; i++) {
                    array[i] = array[i + 1];
                }
                array[array.Length - 1] = -1;
                if (output == -1) { 
                    MessageBox.Show("Error, array empty");
                    return -1;
                } else { 
                    return output;
                }
            }

            public string GetAll() {
                string output = "{ ";
                foreach (int item in array) {
                    output += Convert.ToInt32(item) + ", ";
                }
                return output += "}";
            }

            public void Put(int number) {
                // check if full
                if (array[array.Length - 1] == -1) {
                    // add to array
                    for (int i = 0; i < array.Length; i++) {
                        if (array[i] == -1) {
                            array[i] = number;
                            break;
                        }
                    }
                } else {
                    MessageBox.Show("Entry can't be added, array full");
                }
            }
        }
    }
}
