﻿using System;
using System.Windows;

namespace FIFO_Puffer {
    // 03.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        FifoPuffer p;

        private void Create_Button_Click(object sender, RoutedEventArgs e) {
            try {
                // initiate Object
                p = new FifoPuffer(Convert.ToInt32(Input_Size_Text_Box.Text));

                // enable things
                Input_Text_Box.IsEnabled = true;
                Label_Input.IsEnabled = true;
                Get_Button.IsEnabled = true;
                Get_All_Button.IsEnabled = true;
                Put_Button.IsEnabled = true;
            }
            catch (Exception ex) {
                MessageBox.Show("Something went wrong processing your inputs\n\n" + ex);
                throw;
            }
        }

        private void Put_Button_Click(object sender, RoutedEventArgs e) {
            try {
                p.Put(Convert.ToInt32(Input_Text_Box.Text));
            }
            catch (Exception ex) {
                MessageBox.Show("Something went wrong processing your inputs\n\n" + ex);
                //throw;
            }
        }

        private void Get_Button_Click(object sender, RoutedEventArgs e) {
            Output_Text_Box.Text = p.Get().ToString();
        }

        private void Get_All_Button_Click(object sender, RoutedEventArgs e) {
            Output_Text_Box.Text = p.GetAll();
        }
    }
}
