﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Generate_Quadrat {
    /// 26.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Input_Side_Lenght_Text_Box_TextChanged(object sender, TextChangedEventArgs e) {
            try {
                // read variables
                int length = Convert.ToInt32(Input_Side_Lenght_Text_Box.Text);
                string output = "";
                // generate Square
                for (int i = 0; i < length; i++) {
                    for (int j = 0; j < length; j++) {
                        if (j == 0 || j == length - 1 || i == 0 || i == length - 1) {
                            output += "* ";
                        } else {
                            output += "  ";
                        }
                    }
                    output += "\n";
                }
                // output
                Output_Text_Block.Text = output;
            }
            catch {
                
            }
        }
    }
}
