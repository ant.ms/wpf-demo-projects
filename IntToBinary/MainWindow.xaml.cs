﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IntToBinary
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Input_Text_Box_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                // read and convert input and add it to output
                int input = Convert.ToInt32(Input_Text_Box.Text);
                string output = "";
                while (input >= 1)
                {
                    output += Convert.ToString(input % 2);
                    input /= 2;

                }

                // reverse string
                string theOutput = "";
                for (int i = output.Length - 1; i >= 0; i--)
                {
                    theOutput += Convert.ToString(output[i]);
                }

                Output_Text_Box.Text = theOutput;
            }
            catch
            {
                Console.WriteLine("Something went wrong");
            }
        }
    }
}
