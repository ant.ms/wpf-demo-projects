﻿using System;
using System.Windows;
using System.Windows.Forms;
//imported WMPLib using "add Reference", "COM", "Browse", "C:\Windows\System32\wmp.dll"

namespace MP3_Player {
    // 03.12.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        string[] files;
        int currentlyPlaying = 0;
        WMPLib.WindowsMediaPlayer wplayer = new WMPLib.WindowsMediaPlayer();

        private void Select_Button_Click(object sender, RoutedEventArgs e) {
            // opens the files
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "audio-files (.mp3)|*.mp3|all|*.*";
            openFileDialog.Multiselect = true;
            openFileDialog.ShowDialog();
            try {
                files = openFileDialog.FileNames;
                Current_Playing_Text_Block.Text = "Currently Playing: " + files[0];
            }
            catch {
                Console.WriteLine("ERROR: no file selected");
            }
            // create player
            wplayer.URL = files[currentlyPlaying];
            wplayer.settings.volume = Convert.ToInt32(Input_Slider.Value) * 10;
            wplayer.PlayStateChange += new WMPLib._WMPOCXEvents_PlayStateChangeEventHandler(wplayer_PlayStateChange);
            wplayer.controls.play();
        }

        private void wplayer_PlayStateChange(int NewState) {
            try {
                if (NewState == (int)WMPLib.WMPPlayState.wmppsMediaEnded) {
                    // delete and recreate vplayer
                    currentlyPlaying++;
                    wplayer.controls.stop();
                    wplayer = null;
                    wplayer = new WMPLib.WindowsMediaPlayer();
                    wplayer.settings.volume = Convert.ToInt32(Input_Slider.Value) * 10;
                    wplayer.PlayStateChange += new WMPLib._WMPOCXEvents_PlayStateChangeEventHandler(wplayer_PlayStateChange);
                    Current_Playing_Text_Block.Text = "Currently Playing: " + files[currentlyPlaying];
                    wplayer.URL = files[currentlyPlaying];
                    wplayer.controls.play();
                }
            }
            catch {
                System.Windows.MessageBox.Show("Reached End of play");
            }
        }

        private void Input_Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
            // volume changed
            wplayer.settings.volume = Convert.ToInt32(Input_Slider.Value) * 10;
        }
    }
}
