﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mehrfach_Eingaben
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        int[] counter = new int[9];

        private void Submit_button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                counter[Convert.ToInt32(Zahl_Text_Box.Text)] += 1;
                Zahl_Text_Box.Text = "";
            }
            catch
            {
                MessageBox.Show("invalit input");
            }

            // check if there should be a output
            int checkedIfCount = 0;
            foreach (int item in counter)
            {
                checkedIfCount += item;
            }
            // output
            if (checkedIfCount == 10)
            {
                string output = "Folgende Zahlen wurden mehr als 1x vergeben\n";
                for (int i = 0; i < counter.Length; i++)
                {
                    if (counter[i] > 1)
                    {
                        output += i + " -> " + counter[i] + "x\n";
                    }
                }
                MessageBox.Show(output);
            }
        }
    }
}
