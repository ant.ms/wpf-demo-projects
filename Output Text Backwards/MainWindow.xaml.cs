﻿using System;
using System.Windows;

namespace Output_Text_Backwards {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        

        private void Input_TextBox_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e) {
            Execute();
        }

        private void Capital_CheckBox_Checked(object sender, RoutedEventArgs e) {
            Execute();
        }
        private void Execute() {
            try {
                string input = Input_TextBox.Text;
                string output = "";
                // reverse string
                for (int i = input.Length - 1; i >= 0; i--) {
                    if (Capital_CheckBox.IsChecked == true) {
                        if (Convert.ToString(input[i]).ToUpper() == Convert.ToString(input[i])) {
                            output += Convert.ToString(input[i]).ToLower();
                        } else {
                            output += Convert.ToString(input[i]).ToUpper();
                        }
                    } else {
                        output += Convert.ToString(input[i]);
                    }
                }
                Output_TextBox.Text = output;
            }
            catch (Exception) {

                throw;
            }
        }
    }
}
