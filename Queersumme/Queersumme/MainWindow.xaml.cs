﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Queersumme
{
    /// 25.11.2019, Yanik Ammann
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e)
        {
            int quersumme = 0;
            int input = Convert.ToInt32(Zahl_TextBlock.Text);
            try
            {
                // generate quersumme
                while (input != 0)
                {
                    quersumme += input % 10;
                    input /= 10;
                }

                if (OneDigit_Check_Box.IsChecked == true)
                {
                    int quersummeDuo = quersumme;
                    quersumme = 0;
                    while (quersummeDuo != 0)
                    {
                        quersumme += quersummeDuo % 10;
                        quersummeDuo /= 10;
                    }
                }
                Quersumme_TextBlock.Text = Convert.ToString(quersumme);
            }
            catch
            {
                MessageBox.Show("Something went wrong!\nPlease check your input");
            }
        }
    }
}
