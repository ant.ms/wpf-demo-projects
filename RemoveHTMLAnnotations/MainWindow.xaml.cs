﻿using System.Windows;
using System.Text.RegularExpressions;

namespace RemoveHTMLAnnotations {
    /// 25.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        
        }


        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            string input = Input_Text_Box.Text;
            try {
                // replace
                Regex myRegex = new Regex(@"\<([^\>]+)\>");
                Output_Text_Box.Text = myRegex.Replace(input, "");
            }
            catch {
                MessageBox.Show("whoopsie");
            }
        }
    }
}
