﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Roman_Number {
    /// 03.12.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Input_Text_Box_TextChanged(object sender, TextChangedEventArgs e) {
            try {
                if (Radio_Button_Default.IsChecked == true) {
                    // initiate variables
                    string output = "";
                    int input = Convert.ToInt32(Input_Text_Box.Text);

                    // do magic (aka: math)
                    int ammoutOfRunsForThousands = input / 1000;
                    for (int i = 0; i < ammoutOfRunsForThousands; i++) {
                        output += "M";
                        input -= 1000;
                    }
                    int ammoutOfRunsForFiveHundred = input / 500;
                    for (int i = 0; i < ammoutOfRunsForFiveHundred; i++) {
                        output += "D";
                        input -= 500;
                    }
                    int ammoutOfRunsForHundred = input / 100;
                    for (int i = 0; i < ammoutOfRunsForHundred; i++) {
                        output += "C";
                        input -= 100;
                    }
                    int ammoutOfRunsForFivthy = input / 50;
                    for (int i = 0; i < ammoutOfRunsForFivthy; i++) {
                        output += "L";
                        input -= 50;
                    }
                    int ammoutOfRunsForTen = input / 10;
                    for (int i = 0; i < ammoutOfRunsForTen; i++) {
                        output += "X";
                        input -= 10;
                    }
                    int ammoutOfRunsForFive = input / 5;
                    for (int i = 0; i < ammoutOfRunsForFive; i++) {
                        output += "V";
                        input -= 5;
                    }
                    while (input > 0) {
                        output += "I";
                        input -= 1;
                    }

                    // output
                    Output_Text_Box.Text = output;
                } else {
                    // initiate variables
                    int output = 0;
                    string input = Input_Text_Box.Text.ToUpper() + " ";

                    // do magic (aka: math)
                    while (input[0] == 'M') {
                        input = input.Substring(1);
                        output += 1000;
                    }
                    while (input[0] == 'D') {
                        input = input.Substring(1);
                        output += 500;
                    }
                    while (input[0] == 'C') {
                        input = input.Substring(1);
                        output += 100;
                    }
                    while (input[0] == 'L') {
                        input = input.Substring(1);
                        output += 50;
                    }
                    while (input[0] == 'X') {
                        input = input.Substring(1);
                        output += 10;
                    }
                    while (input[0] == 'V') {
                        input = input.Substring(1);
                        output += 5;
                    }
                    while (input[0] == 'I') {
                        input = input.Substring(1);
                        output += 1;
                    }

                    // output
                    Output_Text_Box.Text = output.ToString();
                }
                
            }
            catch {

            }
        }
    }
}
