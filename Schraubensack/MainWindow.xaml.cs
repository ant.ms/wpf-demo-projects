using System;
using System.IO;
using System.Windows;

namespace Schraubensack {
    // 10.12.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            // initiate variables
            string input = Text_Box.Text;
            int[] schrauben = new int[9];
            for (int i = 0; i < schrauben.Length; i++)
                schrauben[i] = 0;
            int[] mutter = new int[9];
            for (int i = 0; i < schrauben.Length; i++)
                mutter[i] = 0;
            string output = "";

            // read line by line
            using (StringReader reader = new StringReader(input)) {
                string line;
                while ((line = reader.ReadLine()) != null)
                    if (line.Substring(0, 1) == "s")
                        schrauben[Convert.ToInt32(line.Substring(2, 1))]++;
                    else
                        mutter[Convert.ToInt32(line.Substring(2, 1))]++;
            }

            // generate output
            for (int i = 0; i < schrauben.Length; i++)
                if (schrauben[i] != 0 && mutter[i] != 0)
                    output += (i) + "mm: " + Math.Min(mutter[i], schrauben[i]) + " passende Paare\n";

            // output
            Text_Box.Text = output;
        }
    }
}
