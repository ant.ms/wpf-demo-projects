﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SortArray {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            // initialize veriables
            string input = Input_Text_Box.Text.Replace(" ", "");
            // create array
            int[] array = new int[input.Length];
            for (int i = 0; i < input.Length; i++) {
                array[i] = Convert.ToInt32(input[i].ToString());
            }
            // sort array
            for (int i = 0; i <= array.Length - 1; i++) {
                for (int j = i + 1; j < array.Length; j++) {
                    if (array[i] > array[j]) {
                        int tmp = array[i];
                        array[i] = array[j];
                        array[j] = tmp;
                    }
                }
            }
            // output
            string output = "";
            foreach (int item in array) {
                output += item.ToString();
            }
            Output_Text_Box.Text = output;
        }
    }
}
