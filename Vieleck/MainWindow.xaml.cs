﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Vieleck {
    /// 02.12.2019
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            try {
                // read variables
                int point1X = Convert.ToInt32(Input_X_Point_One_Text_Box.Text);
                int point1Y = Convert.ToInt32(Input_Y_Point_One_Text_Box.Text);
                int point2X = Convert.ToInt32(Input_X_Point_Two_Text_Box.Text);
                int point2Y = Convert.ToInt32(Input_Y_Point_Two_Text_Box.Text);
                int point3X = Convert.ToInt32(Input_X_Point_Three_Text_Box.Text);
                int point3Y = Convert.ToInt32(Input_Y_Point_Three_Text_Box.Text);
                int point4X = Convert.ToInt32(Input_X_Point_Four_Text_Box.Text);
                int point4Y = Convert.ToInt32(Input_Y_Point_Four_Text_Box.Text);

                // set variables
                PointF point1 = new System.Drawing.PointF(point1X, point1Y);
                PointF point2 = new System.Drawing.PointF(point2X, point2Y);
                PointF point3 = new System.Drawing.PointF(point3X, point3Y);
                PointF point4 = new System.Drawing.PointF(point4X, point4Y);

                // generate Text output
                string output = "";
                output += "Punkt 1 {X=" + point1X + ",Y=" + point1Y + "}\n";
                output += "Punkt 2 {X=" + point2X + ",Y=" + point2Y + "}\n";
                output += "Punkt 3 {X=" + point3X + ",Y=" + point3Y + "}\n";
                output += "Punkt 4 {X=" + point4X + ",Y=" + point4Y + "}\n";
                // output Text output
                Output_Text_Box.Text = output;

                // calc max size
                int[] maxSize = { point1X, point1Y, point2X, point2Y, point3X, point3Y, point4X, point4Y };
                Array.Sort(maxSize);

                // set Bitmap
                ImageSourceConverter c = new ImageSourceConverter();
                Output_Image.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(Draw(maxSize[maxSize.Length - 1], point1, point2, point3, point4).GetHbitmap(),IntPtr.Zero,Int32Rect.Empty,BitmapSizeOptions.FromEmptyOptions());
            }
            catch (Exception en) {
                MessageBox.Show("Something went wrong while processing your input, please make sure your input is correct and everythign is filled in\n\n" + en.Message);
            }

        }
        
        private Bitmap Draw(int maxSize, PointF point1, PointF point2, PointF point3, PointF point4) {
            // Create Bitmap
            Bitmap bmp = new Bitmap(maxSize, maxSize);
            System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Color.Red, 3);
            Graphics graphics = Graphics.FromImage(bmp);
            // Connect Dots
            graphics.DrawLine(pen, point1, point2);
            graphics.DrawLine(pen, point2, point3);
            graphics.DrawLine(pen, point3, point4);
            graphics.DrawLine(pen, point4, point1);
            return bmp;
        }
    }
}
