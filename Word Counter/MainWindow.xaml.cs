﻿using System.Windows;
using System.Windows.Controls;

namespace Word_Counter {
    /// 26.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Input_Text_Box_TextChanged(object sender, TextChangedEventArgs e) {
            int counter = 1;
            try {
                foreach (char item in Input_Text_Box.Text) {
                    string d = item.ToString();
                    if (d == " " || d == "\t" || d == "\n" || d == "\0") {
                        counter++;
                    }
                }
                Output_Text_Box.Text = counter.ToString();
            }
            catch {
                // do nothing (aka: relax)
            }
        }
    }
}
