﻿using System.Text.RegularExpressions;
using System.Windows;

namespace XHTML_Validieren {
    /// 27.11.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e) {
            string input = Input_TextBox.Text;
            try {
                // replace
                Regex myRegex = new Regex(@"<.{0,}>", RegexOptions.Multiline);
                if (myRegex.Replace(input, "").Replace("\n", "").Replace("\r", "") != "") {
                    MessageBox.Show("Fehler: XML ist nicht gültig\n" + myRegex.Replace(input, ""));
                } else {
                    MessageBox.Show("Sucess: XML ist gültig");
                }
            }
            catch {
                MessageBox.Show("whoopsie");
            }
        }
    }
}
