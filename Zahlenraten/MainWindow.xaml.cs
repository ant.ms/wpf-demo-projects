﻿using System;
using System.Windows;

namespace Zahlenraten
{
    /// 25.11.2019, Yanik Ammann
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            regenNumber();
        }

        int number;
        int versuche = 0;

        void regenNumber() {
            // generate a new random number between 1 and 100
            Random random = new Random();
            number = random.Next(1, 100);
            Submit_Button.Content = "Prüfen";
            versuche = 0;
        }

        private void Submit_Button_Click(object sender, RoutedEventArgs e)
        {
            if (Submit_Button.Content == "Zurücksetzen") {
                // check if has to reset
                Output_Text_Box.Text = "Zahl neu ausgwählt, bereit zum spielen\n";
                regenNumber();
            } else {
                try {
                    // find out the correct answer
                    int input = Convert.ToInt32(Input_Text_Box.Text);
                    if (number == input) {
                        Output_Text_Box.Text = "Zahl stimmt, " + versuche + " Versuche gebraucht";
                        Input_Text_Box.Text = "";
                        Submit_Button.Content = "Zurücksetzen";
                    } else {
                        versuche++;
                        if (number < input) {
                            Output_Text_Box.Text = "Zahl zu gross\n";
                            Input_Text_Box.Text = "";
                        }
                        if (number > input) {
                            Output_Text_Box.Text = "Zahl zu klein\n";
                            Input_Text_Box.Text = "";
                        }
                    }
                }
                catch(Exception ex) {
                    MessageBox.Show("Something went wrong: \n\n"+ex,"Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void Quit_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
