﻿using System.Windows;
using System.Windows.Controls;

namespace morsecode {
    /// 02.12.2019, Yanik Ammann
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }

        char[] alphabet = { ' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 
            't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'ä', 'ö', 'ü' };
        string[] morse = { "  ", "· −", "− · · ·", "− · − ·", "− · ·", "·", "· · − ·", "− − ·", "· · · ·", "· ·", "· − − −", "− · −", 
            "· − · ·", "− −", "− ·", "− − −", "· − − ·", "− − · −", "· − ·", "· · ·", "−", "· · −", "· · · −", "· − −", "− · · −", 
            "− · − −", "− − · ·", "· − − − −", "· · − − −", "· · · − −", "· · · · −", "· · · · ·", "− · · · ·", "− − · · ·", "− − − · ·", 
            "− − − − ·", "− − − − −", "· − · −", "− − − ·", "· · − −" };

        public string GetMorseCode(string input) {
            string output = "";

            // find letter id
            for (int i = 0; i < input.Length; i++)
                for (int j = 0; j < alphabet.Length; j++)
                    if (input[i] == alphabet[j])
                        output += morse[j] + " ";
            
            return output;
        }

        private void Input_Text_Box_TextChanged(object sender, TextChangedEventArgs e) {
            try {
                Output_Text_Box.Text = GetMorseCode(Input_Text_Box.Text.ToLower());
            }
            catch { }
        }
    }
}
